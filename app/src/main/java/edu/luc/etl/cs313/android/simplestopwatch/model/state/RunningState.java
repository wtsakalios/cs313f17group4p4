package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;

class RunningState implements StopwatchState {

    public RunningState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }

    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionSetTicks(0);
        sm.actionStop();
        sm.toStoppedState();
        sm.actionUpdateView();
    }

    //@Override
    //public void onLapReset() {
    //    sm.actionLap();
    //    sm.toLapRunningState();
    //}

    @Override
    public void onTick() {
        if (sm.actionGetTicks() > 0) {
            sm.actionDec();
        }
        else {
            sm.toAlarmingState();
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
