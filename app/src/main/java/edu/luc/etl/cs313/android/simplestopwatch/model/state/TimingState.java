package edu.luc.etl.cs313.android.simplestopwatch.model.state;

/**
 * Created by BillyTsakalios on 12/11/17.
 */

import edu.luc.etl.cs313.android.simplestopwatch.R;
import edu.luc.etl.cs313.android.simplestopwatch.android.StopwatchAdapter;

public class TimingState extends StopwatchAdapter implements StopwatchState {

    private final StopwatchSMStateView sm;
    private final int MAX_COUNT = 99;
    private int ticks;

    public TimingState (final StopwatchSMStateView sm) {
        this.sm = sm;
        ticks = 0;
    }

    @Override public void onStartStop() {
        if (sm.actionGetTicks() < MAX_COUNT){
            ticks = 0;
            sm.actionInc();
        }
        else {
            sm.actionBeep();
            sm.toRunningState();
        }
    }

    @Override public void onTick() {
        this.ticks++;
        if (ticks == 3) {
            sm.actionBeep();
            sm.toRunningState();
        }
    }

    @Override public void updateView() {
        sm.updateUIRuntime();
    }

    @Override public int getId() {
            return R.string.TIMING;
        }
    }
