package edu.luc.etl.cs313.android.simplestopwatch.model.state;

import edu.luc.etl.cs313.android.simplestopwatch.R;
/**
 * Created by BillyTsakalios on 12/9/17.
 */

public class AlarmingState implements StopwatchState {

    public AlarmingState(final StopwatchSMStateView sm) {
        this.sm = sm;
    }
    private final StopwatchSMStateView sm;

    @Override
    public void onStartStop() {
        sm.actionSetTicks(0);
        sm.actionStop();
        sm.toStoppedState();
    }

    @Override
    public void onTick() {
        sm.actionBeep();
    }

    @Override public void updateView () {int t = 0;}

    @Override public int getId () { return R.string.ALARMING; }
}
