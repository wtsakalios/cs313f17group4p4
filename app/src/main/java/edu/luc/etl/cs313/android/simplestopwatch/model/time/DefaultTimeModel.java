package edu.luc.etl.cs313.android.simplestopwatch.model.time;

import static edu.luc.etl.cs313.android.simplestopwatch.common.Constants.*;


/**
 * An implementation of the stopwatch data model.
 */
public class DefaultTimeModel implements TimeModel {

    private int Runtime = 0;
    private int ticks = 0;

    @Override
    public void resetRuntime() {
        Runtime = 0;
    }

    @Override
    public void incRuntime() {
        Runtime++;
    }

    @Override public void decRuntime() {
        Runtime--;
    }

    @Override
    public int getRuntime() {
        return Runtime;
    }

    @Override
    public void setRuntime (int ticks) {
        Runtime = ticks;
    }
}