package edu.luc.etl.cs313.android.simplestopwatch.common;

/**
 * Constants for the time calculations used by the stopwatch.
 */
public final class Constants {

    public static int SEC_PER_TICK = 1;
    public static int SEC_PER_MIN  = 60;
    public static int SEC_PER_HOUR = 3600;
    //public static int max_count = 99;
    //public static int timeout = 3;
    //public static int short_beep = 2;
    //public static int long_beep = 400;

    private Constants() { }
}